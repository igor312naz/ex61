<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Dish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDishData extends Fixture implements DependentFixtureInterface
{
    private static $dishes = [];

    public function load(ObjectManager $manager)
    {
        $namesOfDishes = [
            "Манты",
            "Лагман",
            "Гуляш",
            "Шашлык",
            "Плов",
            "Пельмени",
            "Голубцы",
            "Мясо по-итальянски",
            "Самсы",
            "Бифштекс"
        ];
        for ($i = 1; $i <= 10; $i++) {
            $dish = new Dish();
            $dish
                ->setName($namesOfDishes[$i - 1])
                ->setPrice($this->getRandomPrice())
                ->setImage("dish{$i}");

            if($i < 6) {
                $dish->setPlace($this->getReference(LoadPlaceData::PLACE_ONE));
            } else {
                $dish->setPlace($this->getReference(LoadPlaceData::PLACE_TWO));
            }
            $manager->persist($dish);
            $dishIdent = "dish{$i}";
            $this->addReference($dishIdent, $dish);
            self::$dishes[] = $dishIdent;
        }

        $manager->flush();

    }

    function getDependencies()
    {
        return array(
            LoadPlaceData::class
        );
    }

    public function getRandomPrice() {
        return rand(10, 30) * 10;
    }

    public static function getDishes() {
        return self::$dishes;
    }
}
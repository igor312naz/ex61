<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPlaceData extends Fixture
{
    public const PLACE_ONE = 'Ала-Тоо';
    public const PLACE_TWO = 'Фаиза';

    public function load(ObjectManager $manager)
    {
        $place1 = new Place();
        $place1
            ->setName('Ала-Тоо')
            ->setDescription('"Ала-Тоо" — клуб, ресторан, летнее кафе и аквапарк на Южной Магистрали. В меню ресторана, клуба и кафе представлена европейская и национальная кухня')
            ->setImage('alatoo.jpg');

        $manager->persist($place1);

        $place2 = new Place();
        $place2
            ->setName('Фаиза')
            ->setDescription('"Фаиза" - сеть кафе, заслужившая известность далеко за пределами страны, благодаря превосходной кухне, первоклассному обслуживанию и вниманию к каждому гостю')
            ->setImage('faiza.jpg');

        $manager->persist($place2);
        $manager->flush();

        $this->addReference(self::PLACE_ONE, $place1);
        $this->addReference(self::PLACE_TWO, $place2);
    }
}

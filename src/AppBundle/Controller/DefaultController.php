<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $places = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Place')
            ->findAll();

        $dishesByPlaces = [];

        foreach($places as $place) {
            $dishes = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Dish')
                ->getMostPopularDishesByPlace($place, 2);

            $dishesByPlaces[] = [
                'place' => $place,
                'dishes' => $dishes
            ];
        }

        dump($dishesByPlaces[0]['dishes']);




        return $this->render('@App/Base/index.html.twig', array(
            'place1'=>$dishesByPlaces[0]['place'],
            'place2'=>$dishesByPlaces[1]['place'],
            'dishes1'=>$dishesByPlaces[0]['dishes'],
            'dishes2'=>$dishesByPlaces[1]['dishes'],
        ));

    }
}

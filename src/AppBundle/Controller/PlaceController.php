<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PlaceController extends Controller
{
    /**
     * @Route("/place/{id}",requirements={"id": "\d+"}, name="place")
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $id)
    {

        $dishes = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Dish')
            ->getAllDishes($id);

        dump($dishes);

        return $this->render('@App/Base/place.html.twig',array(
            'dishes'=>$dishes
        ));

    }
}

<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Place;

/**
 * DishRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DishRepository extends \Doctrine\ORM\EntityRepository
{
    public function getMostPopularDishesByPlace(Place $place, $amount) {
        return $this
            ->createQueryBuilder('d')
            ->select('d dish')
            ->addSelect('count(p) amount')
            ->innerJoin('d.purchases','p')
            ->where('d.place = :place')
            ->andWhere('p.date BETWEEN \'2018-01-01\' AND CURRENT_DATE() ')
            ->setParameter('place', $place)
            ->groupBy('d')
            ->orderBy('amount', 'DESC')
            ->setMaxResults($amount)
            ->getQuery()
            ->getResult( );
    }

    public function getAllDishes($id) {
        dump($id);
        return $this
            ->getEntityManager()
            ->createQuery('select d.name,d.image from AppBundle\Entity\Dish d where d.place='.$id)
            ->getResult();
    }
}
